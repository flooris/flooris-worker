<?php

use Dotenv\Dotenv;

if( file_exists(__DIR__ . '/../.env') ) {
    $dotenv = new Dotenv(__DIR__ . '/../');
    $dotenv->load();
}

if( ! function_exists('env') ) {
    function env($key_name, $default = '')
    {
        if ( ! array_key_exists($key_name, $_ENV) ) {
            return $default;
        }

        return getenv($key_name);
    }
}
