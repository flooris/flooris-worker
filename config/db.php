<?php

return array(
    'connection' => array(
        'host'     => env('DB_HOST', 'localhost'),
        'port'     => env('DB_PORT', 3306),
        'database' => env('DB_DATABASE', 'worker'),
        'username' => env('DB_USER', 'root'),
        'password' => env('DB_PASSWD', '')
    )
);
