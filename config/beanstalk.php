<?php

return array(
    'host' => env('QUEUE_SERVER', 'localhost'),
    'port' => 11300,
    'timeout' => 5,
    'queue' => env('QUEUE_NAME', 'default'),
    'max_reserves' => 5
);
