<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/helpers/helpers.php';

$app = require __DIR__ . '/app/Console/Kernel.php';

$app->run();
