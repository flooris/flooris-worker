<?php

use Flooris\Queue\Command\QueueWorkerCommand;
use Symfony\Component\Console\Application;

$application = new Application;
$application->add(new QueueWorkerCommand);

// Add your custom worker commands below
// $application->add(new WorkThatQueueCommand);

return $application;